# Created by Yuxin Zhang at 14/7/21
import time
import pandas as pd
from scipy.signal import savgol_filter, spectrogram
import PySimpleGUI as sg
import os.path

sg.theme('Dark Blue 3')


def segmentation(filepath, window_size, overlap_rate):
    data_sep_mark = '\t'
    df = pd.read_csv(filepath, sep=data_sep_mark)
    signal = df['Current (A)'].to_numpy()
    print("******")
    print(len(signal))
    print("******")
    signal_segments = []
    for i in range(0, len(signal), int(window_size * (1 - overlap_rate))):
        if i + window_size > len(signal):
            break
        signal_segments.append(signal[i: i + window_size])
    return signal_segments


def detect_freq(sig_seg, lowest_freq=60, fs=10e3):
    print("detect freq")
    de_sig = savgol_filter(sig_seg, 7, 3)
    f, t, Sxx = spectrogram(de_sig, fs, nperseg=256, noverlap=128, mode="magnitude")
    main_frequencies = []
    for idx, timestamp in enumerate(t):
        S_values = []
        for idx2, freq in enumerate(f):
            S_values.append(Sxx[idx2][idx])
        max_index = S_values.index(max(S_values))
        if f[max_index] > lowest_freq:
            main_frequencies.append(f[max_index])
        else:
            main_frequencies.append(0)
    most_common_freq = max(main_frequencies, key=main_frequencies.count)
    # print(main_frequencies)
    print(most_common_freq)
    return most_common_freq


file_list_column = [
    [
        sg.Text("File Folder"),
        sg.In(size=(25, 1), enable_events=True, key="-FOLDER-"),
        sg.FolderBrowse(),
    ],
    [
        sg.Listbox(
            values=[], enable_events=True, size=(40, 10), key="-FILE LIST-"
        )
    ],
    [
        sg.Button("Read", key="-READ FILE-"), sg.Button("Stop", key="Exit")
    ],
    [
        sg.Text(size=(40, 2), key="-FreqOUT-")
    ],
]

image_viewer_column = [
    [sg.Text("Detected Image", size=(25, 1))],
    # [sg.Text(size=(25, 1), key="-TOUT-")],
    [sg.Image(key="-IMAGE-")],
]

# ----- Full layout -----
layout = [
    [
        sg.Column(file_list_column),
        sg.VSeperator(),
        sg.Column(image_viewer_column),
    ]
]

window = sg.Window("Freq Detector", layout)
filename = ""
# Run the Event Loop
while True:
    event, values = window.read(timeout=100)
    if event == "Exit" or event == sg.WIN_CLOSED:
        break
    # Folder name was filled in, make a list of files in the folder
    if event == "-FOLDER-":
        folder = values["-FOLDER-"]
        try:
            # Get list of files in folder
            file_list = os.listdir(folder)
        except:
            file_list = []

        fnames = [
            f
            for f in file_list
            if os.path.isfile(os.path.join(folder, f))
               and f.lower().endswith((".txt", ".log"))
        ]
        window["-FILE LIST-"].update(fnames)
    elif event == "-FILE LIST-":  # A file was chosen from the listbox
        try:
            filename = os.path.join(
                values["-FOLDER-"], values["-FILE LIST-"][0]
            )
        except:
            pass
    elif event == "-READ FILE-":
        if filename == "":
            break
        # window["-TOUT-"].update(filename)
        sig_segs = segmentation(filename, 1024, 0.5)
        print(len(sig_segs))
        for sig_seg in sig_segs:
            event, values = window.read(timeout=100)
            if event == "Exit" or event == sg.WIN_CLOSED:
                break
            freq = detect_freq(sig_seg)
            if freq == 0:
                window["-FreqOUT-"].update("Detecting Frequency: \r <60 Hz")
                window["-IMAGE-"].update(filename="figures/no.png")
            elif 190 < freq < 210:
                window["-FreqOUT-"].update(f"Detecting Frequency: \r {freq} Hz")
                window["-IMAGE-"].update(filename="figures/200.png")
            elif 440 < freq < 470:
                window["-FreqOUT-"].update(f"Detecting Frequency: \r {freq} Hz")
                window["-IMAGE-"].update(filename="figures/450.png")
            elif 490 < freq < 510:
                window["-FreqOUT-"].update(f"Detecting Frequency: \r {freq} Hz")
                window["-IMAGE-"].update(filename="figures/500.png")
            elif 1380 < freq < 1420:
                window["-FreqOUT-"].update(f"Detecting Frequency: \r {freq} Hz")
                window["-IMAGE-"].update(filename="figures/1400.png")
            else:
                window["-FreqOUT-"].update(f"Detecting Frequency: \r {freq} Hz")
                window["-IMAGE-"].update(filename="figures/no.png")
        window["-FreqOUT-"].update(
            "Frequency Detection Finished !\rPlease select a new file to continue !")
window.close()
